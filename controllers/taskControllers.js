const req = require('express/lib/request')
const res = require('express/lib/response')
let Task = require('./../models/Task')
module.exports.createTask = async(req,res) =>{
    await Task.findOne(
        {name:req.body.name}
    ).then((result)=>{
       if(result===null){
           Task.create({name:req.body.name})
           res.send("Task has been successfully created")
       }else{
            res.send("Duplicated Task")
       }
    })

}


module.exports.getAllTask= async(req,res)=>{
    try{
    await Task.find().then((result)=>{
        res.send(result)
    })   
    }catch(e){
        console.log(e)
    }
}
//////////activity
module.exports.getTask= async(req,res)=>{
    try{
      
    await Task.findById(req.params.id).then(result=>
        res.send(result)
    )
    }catch(err){
        res.send("Document does not exist")
    }
}

module.exports.updateTask = async(req,res)=>{
    await Task.findByIdAndUpdate(req.params.id,{$set:{status:"completed"}},{new:true})
    .then(result=>res.send(result))
}