const express = require('express')
const app = express();
const port = 3000;
const mongoose = require('mongoose')
const dotenv = require('dotenv')
dotenv.config();
app.use(express.json())

const Task = require('./models/Task')

const taskRoutes = require('./routes/taskRoutes')
app.use(`/api/tasks`,taskRoutes)


mongoose.connect(process.env.MONGO);
const db = mongoose.connection;
db.on('error', console.error.bind(console,'connection error:'))
db.once('open',()=>console.log(`Connected to Database`))


app.listen(port,()=>{
    console.log(`Connected to port ${port}`)
})