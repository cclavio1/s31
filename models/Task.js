const mongoose= require('mongoose')
const taskSchema = new mongoose.Schema(
    {
        name: {
            type: String,
            required:[true,`Name is required`]
        },
        status:{
            type:String,
            default:"pending"
        }
    }
)
const Task = mongoose.model("task",taskSchema);


module.exports = Task;