const express = require('express')
const router = express.Router();
const {createTask,getAllTask,deleteTask,updateTask,getTask} = require('./../controllers/taskControllers')

router.post('/',async(req,res)=>{
    await createTask(req,res);
})
router.get('/',async(req,res)=>{
    await getAllTask(req,res);
})
router.delete('/:id/delete',async(req,res)=>{
    console.log("delete")
    await deleteTask(req,res);
})
router.get('/:id',async(req,res)=>{
    await getTask(req,res);
});
router.put('/:id/complete',async(req,res)=>{
    await updateTask(req,res);
})
module.exports = router;